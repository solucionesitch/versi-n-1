-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-12-2016 a las 16:29:02
-- Versión del servidor: 10.1.8-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `presupuestopersonal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_basicos`
--

CREATE TABLE `categoria_basicos` (
  `Id` int(11) NOT NULL,
  `Categoria` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria_basicos`
--

INSERT INTO `categoria_basicos` (`Id`, `Categoria`) VALUES
(1, 'Aliños-Especies-Condimentos'),
(2, 'Enlatados'),
(3, 'Granos'),
(4, 'Repostreria-Postres-Meriendas'),
(5, 'Otros Víveres-Varios'),
(6, 'Bebidas'),
(7, 'Embutidos-Charcuteria'),
(8, 'Artículos para la limpieza'),
(9, 'Artículos de tocador'),
(10, '"Otros"'),
(11, 'Carnes'),
(12, 'Hortalizas, verduras, frutas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_fijos`
--

CREATE TABLE `categoria_fijos` (
  `Id` int(11) NOT NULL,
  `Categoria` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria_fijos`
--

INSERT INTO `categoria_fijos` (`Id`, `Categoria`) VALUES
(1, 'Renta/hipoteca'),
(2, 'Gas'),
(3, 'Luz'),
(4, 'Agua'),
(5, 'Telefono'),
(6, 'Prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_otros`
--

CREATE TABLE `categoria_otros` (
  `Id` int(11) NOT NULL,
  `Categoria` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria_otros`
--

INSERT INTO `categoria_otros` (`Id`, `Categoria`) VALUES
(1, 'Matenimiento del carro'),
(2, 'Vacaciones'),
(3, 'Mascotas'),
(4, 'Diversión');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compratiemporeal`
--

CREATE TABLE `compratiemporeal` (
  `Id` int(11) NOT NULL,
  `Gasto` varchar(100) NOT NULL,
  `Total` int(11) NOT NULL,
  `IdUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compratiemporeal`
--

INSERT INTO `compratiemporeal` (`Id`, `Gasto`, `Total`, `IdUser`) VALUES
(16, 'Maiz dorado', 36, 0),
(17, 'Hotel', 500, 0),
(18, 'Renta/hipoteca', 600, 0),
(20, 'Renta/hipoteca', 600, 0),
(21, 'Renta/hipoteca', 600, 0),
(22, 'Neftalina', 600, 0),
(23, 'Neftalina', 600, 0),
(24, 'Neftalina', 600, 0),
(25, 'Neftalina', 600, 0),
(26, 'Neftalina', 600, 0),
(27, 'Neftalina', 600, 0),
(28, 'Neftalina', 600, 0),
(29, 'Neftalina', 600, 0),
(30, 'Hotel', 0, 0),
(31, 'Neftalina', 600, 0),
(32, 'Hotel', 500, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_usuario`
--

CREATE TABLE `lista_usuario` (
  `Id` int(11) NOT NULL,
  `Gasto` varchar(100) NOT NULL,
  `Valor` int(11) NOT NULL,
  `IdUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lista_usuario`
--

INSERT INTO `lista_usuario` (`Id`, `Gasto`, `Valor`, `IdUser`) VALUES
(1, 'Luz', 0, 1),
(2, 'prueba', 132, 1),
(3, 'ojo', 1234, 1),
(4, 'Agua', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `places`
--

CREATE TABLE `places` (
  `id_place` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `IdUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `places`
--

INSERT INTO `places` (`id_place`, `nombre`, `lat`, `lon`, `IdUser`) VALUES
(2, 'qewr', 20.97439823049979, -89.61785610361329, 1),
(4, 'Nuevo', 20.95708621343285, -89.61751278085939, 1),
(9, 'Hola', 20.96013199134572, -89.61682613535157, 1),
(10, 'qwerqwer', 20.965101285315942, -89.61957271738282, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria_basicos`
--

CREATE TABLE `subcategoria_basicos` (
  `Id` int(11) NOT NULL,
  `Subcategoria` varchar(100) NOT NULL,
  `Id_Categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategoria_basicos`
--

INSERT INTO `subcategoria_basicos` (`Id`, `Subcategoria`, `Id_Categoria`) VALUES
(1, 'Sal', 1),
(2, 'Ajo en polvo', 1),
(3, 'Cebolla en polvo', 1),
(4, 'Tomillo', 1),
(5, 'Salvia, Mejorana', 1),
(6, 'Orégano', 1),
(7, 'Pimienta', 1),
(8, 'Curry', 1),
(9, 'Laurel en Hojas', 1),
(10, 'Comino', 1),
(11, 'Color Onoto', 1),
(12, 'Colorante paella', 1),
(13, 'Cubitos pollo', 1),
(14, 'Cubitos carne', 1),
(15, 'Cubitos pescado', 1),
(16, 'Cubitos tocineta', 1),
(17, 'Maiz dorado', 2),
(18, 'Consomé', 2),
(19, 'Vegetales mixtos', 2),
(20, 'Petit poies', 2),
(21, 'Caraotas negras', 2),
(22, 'Tomates naturales', 2),
(23, 'Jamón Endiablado', 2),
(24, 'Atún', 2),
(25, 'Sardinas', 2),
(26, 'Anchoas', 2),
(27, 'Cocktail frutas', 2),
(28, 'Leche en polvo', 2),
(29, 'Leche condensada', 2),
(30, 'Concentrados de fruta', 2),
(31, 'Mantequilla en lata', 2),
(32, 'Pasapalos en lata', 2),
(33, 'Lentejas', 3),
(34, 'Arbejitas verdes', 3),
(35, 'Garbanzos', 3),
(36, 'Caraotas rojas', 3),
(37, 'Caraotas blancas', 3),
(38, 'Caraotas negras', 3),
(39, 'Quinchonchos', 3),
(40, 'Frijoles', 3),
(41, 'Arbejitas congeladas (bolsa)', 3),
(42, 'Azúcar', 4),
(43, 'Azúcar morena', 4),
(44, 'Azúcar pulverizada', 4),
(45, 'Polvo de hornear', 4),
(46, 'Levadura', 4),
(47, 'Nuez Moscada', 4),
(48, 'Canela (en polvo y en rama)', 4),
(49, 'Cremor tártaro', 4),
(50, 'Guayabita', 4),
(51, 'Fécula de maíz', 4),
(52, 'Confites adorno', 4),
(53, 'Vainilla', 4),
(54, 'Colorantes artificiales', 4),
(55, 'Frutas glaseadas', 4),
(56, 'Pasas, guindas', 4),
(57, 'Gelatinas', 4),
(58, 'Chocolate polvo', 4),
(59, 'Sirop (fresa, mora, chocolate)', 4),
(60, 'Galletas', 4),
(61, 'Cereales', 4),
(62, 'Helado', 4),
(63, 'Yogurt firme', 4),
(64, 'Puré de papas', 5),
(65, 'Harina de Trigo', 5),
(66, 'Harina de Maíz', 5),
(67, 'Afrecho', 5),
(68, 'Pan rallado', 5),
(69, 'Avena hojuela', 5),
(70, 'Arroz', 5),
(71, 'Pasta al huevo', 5),
(72, 'Pasta de sémola', 5),
(73, 'Huevos', 5),
(74, 'Panquecas', 5),
(75, 'Harina de avena', 5),
(76, 'Aceite', 5),
(77, 'Vinagre', 5),
(78, 'Vino cocinar', 5),
(79, 'Salsa inglesa', 5),
(80, 'Salsa Soya', 5),
(81, 'Salsa de Tomate', 5),
(82, 'Pasta tomate concentrada', 5),
(83, 'Mayonesa', 5),
(84, 'Mostaza', 5),
(85, 'Pastas para untar', 5),
(86, 'Alcaparras', 5),
(87, 'Aceitunas', 5),
(88, 'Mermeladas, miel', 5),
(89, 'Café en polvo', 6),
(90, 'Té negro', 6),
(91, 'Té Manzanilla', 6),
(92, 'Té de Tilo', 6),
(93, 'Té de Menta', 6),
(94, 'Café instant.', 6),
(95, 'Taco, Ricomalt', 6),
(96, 'Jugos de frutas', 6),
(97, 'Yogurt líquido', 6),
(98, 'Refrescos', 6),
(99, 'Jugo de Naranja', 6),
(100, 'Cervezas', 6),
(101, 'Aguakina/Soda', 6),
(102, 'Agua Potable', 6),
(103, 'Licores', 6),
(104, 'Parmesano', 7),
(105, 'Pecorino', 7),
(106, 'Queso blanco', 7),
(107, 'Queso amarillo', 7),
(108, 'Jamón', 7),
(109, 'Bologna', 7),
(110, 'Salchichón', 7),
(111, 'Pasta de hígado', 7),
(112, 'Salchichas', 7),
(113, 'Tocineta', 7),
(114, 'Margarina Suave', 7),
(115, 'Crema de leche', 7),
(116, 'Queso crema', 7),
(117, 'Jabón de lavar', 8),
(118, 'Blanqueador', 8),
(119, 'Limpiador de pocetas', 8),
(120, 'Detergente líq./polvo', 8),
(121, 'Cloro', 8),
(122, 'Limpiador de muebles', 8),
(123, 'Jabón lavaplatos', 8),
(124, 'Pañitos secar', 8),
(125, 'Esponjitas', 8),
(126, 'Cepillos lavar', 8),
(127, 'Servilletas', 8),
(128, 'Toallin absorbente', 8),
(129, 'Papel encerado', 8),
(130, 'Papel aluminio', 8),
(131, 'Papel plástico envolvente', 8),
(132, 'Jabón tocador', 9),
(133, 'Champú él/ella', 9),
(134, 'Enjuague', 9),
(135, 'Desodorontes', 9),
(136, 'Toallas sanit.', 9),
(137, 'Papel higiénico', 9),
(138, 'Hojillas afeitar', 9),
(139, 'Espuma afeitar', 9),
(140, 'Pasta dental', 9),
(141, 'Removedor de esmalte', 9),
(142, 'Insecticida', 10),
(143, 'Fumigación matas', 10),
(144, 'Abono para matas', 10),
(145, 'Neftalina', 10),
(146, 'Kerosene', 10),
(147, 'Bombillos varios', 10),
(148, 'Velas, fósforos', 10),
(149, 'Betún p/zapatos', 10),
(150, 'Pulitura para Bronce', 10),
(151, 'Pulitura para Plata', 10),
(152, 'Rollo de pavilo', 10),
(153, 'Matamoscas', 10),
(154, 'Molida', 11),
(155, 'Milanesas', 11),
(156, 'Bistec', 11),
(157, 'Guisar', 11),
(158, 'Medallones', 11),
(159, 'Lagarto', 11),
(160, 'Falda', 11),
(161, 'Muchacho redondo', 11),
(162, 'Para pinchos', 11),
(163, 'Roastbeef', 11),
(164, 'Chuletas', 11),
(165, 'Cochino', 11),
(166, 'Pollo', 11),
(167, 'Pescado', 11),
(168, 'Tomates, pimentones', 12),
(169, 'Zanahorias', 12),
(170, 'Papas, apio', 12),
(171, 'Auyama', 12),
(172, 'Cebollas/cebollín', 12),
(173, 'Calabacines', 12),
(174, 'Yuca, Ñame', 12),
(175, 'Pepino', 12),
(176, 'Ají, Ajos', 12),
(177, 'Lechuga, repollo', 12),
(178, 'Espinacas', 12),
(179, 'Compuesto (perejil, cilantro...)', 12),
(180, 'Plátanos', 12),
(181, 'FRUTAS (manzanas, patilla, lechosa, cambur, toronjas, mandarinas, mangos, limones, melón, uvas, fres', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria_otros`
--

CREATE TABLE `subcategoria_otros` (
  `Id` int(11) NOT NULL,
  `Subcategoria` varchar(100) NOT NULL,
  `Id_Categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategoria_otros`
--

INSERT INTO `subcategoria_otros` (`Id`, `Subcategoria`, `Id_Categoria`) VALUES
(1, 'Frenos', 1),
(2, 'Lubricantes', 1),
(3, 'Gasolina', 1),
(4, 'Amortiguadores', 1),
(5, 'Carrocería', 1),
(6, 'Hotel', 2),
(7, 'Boletos', 2),
(8, 'Comida', 2),
(9, 'Visa', 2),
(10, 'Gasolina', 2),
(11, 'Alimento', 3),
(12, 'Baños', 3),
(13, 'Juguetes', 3),
(14, 'Veterinario', 3),
(15, 'Ropa', 3),
(16, 'Videojuegos', 4),
(17, 'Cine', 4),
(18, 'Centro nocturno', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_pass`
--

CREATE TABLE `user_pass` (
  `Id_UserPass` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `Ingreso` int(11) DEFAULT NULL,
  `temp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_pass`
--

INSERT INTO `user_pass` (`Id_UserPass`, `username`, `password`, `Ingreso`, `temp`) VALUES
(1, 'CarlosRomero', 'romerito84', 1500, 0),
(2, 'RaulUc', 'aguayo', 50000, 0),
(3, 'AndresMadariaga', 'aguilar', NULL, 0),
(4, 'ErickOmar', 'pily', NULL, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria_basicos`
--
ALTER TABLE `categoria_basicos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `categoria_fijos`
--
ALTER TABLE `categoria_fijos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `categoria_otros`
--
ALTER TABLE `categoria_otros`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `compratiemporeal`
--
ALTER TABLE `compratiemporeal`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `lista_usuario`
--
ALTER TABLE `lista_usuario`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id_place`);

--
-- Indices de la tabla `subcategoria_basicos`
--
ALTER TABLE `subcategoria_basicos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `subcategoria_otros`
--
ALTER TABLE `subcategoria_otros`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `user_pass`
--
ALTER TABLE `user_pass`
  ADD PRIMARY KEY (`Id_UserPass`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria_basicos`
--
ALTER TABLE `categoria_basicos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `categoria_fijos`
--
ALTER TABLE `categoria_fijos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `categoria_otros`
--
ALTER TABLE `categoria_otros`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `compratiemporeal`
--
ALTER TABLE `compratiemporeal`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `lista_usuario`
--
ALTER TABLE `lista_usuario`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `places`
--
ALTER TABLE `places`
  MODIFY `id_place` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `subcategoria_basicos`
--
ALTER TABLE `subcategoria_basicos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT de la tabla `subcategoria_otros`
--
ALTER TABLE `subcategoria_otros`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `user_pass`
--
ALTER TABLE `user_pass`
  MODIFY `Id_UserPass` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
