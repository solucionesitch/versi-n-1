<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema Presupuesto</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

        <!-- Custom CSS -->
        <link href="<?php echo base_url('css/landing-page.css')?>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url('font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <link  type="text/css"  rel="stylesheet" href="<?php echo base_url('css/styleLogin.css')?>">

        <link href="<?php echo base_url('css/style.css')?>" rel="stylesheet" />

        <!-- jQuery -->
        <script src="<?php echo base_url('js/jquery.js')?>"></script>
        <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        width: 100%;
        height: 80%;
      }
      #coords{width: 500px;}
    </style>
  </head>
  <body >
          <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand topnav" href="<?php echo site_url('Sistema/System')?>">Presupuesto Personal</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Opciones <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo site_url('Sistema/System')?>">Compra Tiempo Real</a>
                  </li>

                  <li>
                    <a href=<?php echo site_url('User/index')?>>Preparación para las compras</a>
                  </li>

                  <li>
                    <a href="<?php echo site_url('User/CompraLista')?>">Compra con lista</a>
                  </li>

                  <li>
                    <a href="<?php echo site_url('User/provee')?>">Proveedores</a>
                  </li>

                  <li>
                    <a href=<?php echo site_url('User/mapAgrega'); ?>>Agregar proveedor</a>
                  </li>
                </ul>
              </li>

              <li><a href="<?php echo site_url('Sistema/logout')?>">Cerrar Sesión</a></li>
            </ul>
          </div>
        <!-- /.navbar-collapse -->
        </div>
      <!-- /.container -->
      </nav>

    <div id="map"></div>

    <?php echo form_open('User/posteo'); ?>
        <input type="text" id="lat" name="lat" />
        <br><input type="text" id="lon" name="lon" />
        <br><input type="text" placeholder="Nombre del sitio" name="nombre" />
        <br><input type="submit" value="Guardar Sitio">
    </form> 
    <script>
 
 
var marker;          //variable del marcador
var coords = {};    //coordenadas obtenidas con la geolocalización
 
//Funcion principal
initMap = function ()
{
 
    //usamos la API para geolocalizar el usuario
        navigator.geolocation.getCurrentPosition(
          function (position){
            coords =  {
              lng: position.coords.longitude,
              lat: position.coords.latitude
            };
            setMapa(coords);  //pasamos las coordenadas al metodo para crear el mapa
            
          
          },function(error){console.log(error);});
    
}
 
 
 
function setMapa (coords)
{  
      //Se crea una nueva instancia del objeto mapa
      var map = new google.maps.Map(document.getElementById('map'),
      {
        zoom: 13,
        center:new google.maps.LatLng(coords.lat,coords.lng),
 
      });
 
      //Creamos el marcador en el mapa con sus propiedades
      //para nuestro obetivo tenemos que poner el atributo draggable en true
      //position pondremos las mismas coordenas que obtuvimos en la geolocalización
      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(coords.lat,coords.lng),
 
      });
      //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica
      //cuando el usuario a soltado el marcador
      marker.addListener('click', toggleBounce);
      
      marker.addListener( 'dragend', function (event)
      {
        //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
        document.getElementById("lat").value = this.getPosition().lat();
        document.getElementById("lon").value = this.getPosition().lng();
      });
}
 
//callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
 
// Carga de la libreria de google maps
 
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyA9Wpp7_aZf8tQ_Nh2odQ6aZVb9tsFjFpo"></script>
  </body>
</html>