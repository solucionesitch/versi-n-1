<!DOCTYPE html>
<html lang="en">

<!-- Equipo 3
Integrantes
Jose Carlos Romero Vera
Andres Aguilar Madariaga
Erick OMar Vazquez Batun
Raul ALberto Uc Aguayo -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Presupuesto Personal</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('css/landing-page.css')?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url('font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link  type="text/css"  rel="stylesheet" href="<?php echo base_url('css/styleLogin.css')?>"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav">BIENVENIDOS</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#inicio" data-toggle="tab">Inicio</a>
                    </li>
                    
                      <li>
                        <a href="#registrarse" data-toggle="tab">Registrarse</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>    
    <!--Ventana de Pestañas -->
    
    <div class="container">
        <div class="tab-content">
            <div class="tab-pane fade in active" id="inicio" >
                    <div class="intro-header" >
                <div class="container">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="intro-message">
                                <h1>Sistema de Planeación de Presupuesto</h1>

                                <hr class="intro-divider">
                                <div class="container">
                        <!-- <form class="form-horizontal" action=""> -->
                        <?php echo validation_errors(); 
						    if(isset($error)){
						    	echo "<p>".$error."</p>";
						    }
						    $data = array('class' => 'form-horizontal', );
						    echo form_open('Sistema/User', $data); ?>
                            
                            <div class="form-group">
                                <label for ="Nombre" class="control-label col-md-5" >Usuario</label>
                                <div class=" col-md-3">
                                    <input class="form-control" id="clvAlumno" type="text" placeholder="Nombre" name="username" required="true" />
                                </div>

                                <label for ="Nombre" class="control-label col-md-5" >Contraseña</label>
                                <div class=" col-md-3">
                                    <input class="form-control" id="clvAlumno" type="password" placeholder="Contraseña" name="passwordlogin" required="true" />
                                     <br>
                                     <!--<button class="btn btn-primary">Login</button>-->
                                     <input type="submit" name="entrar" value="Entrar" class="btn btn-primary">
                                </div>    
                            </div>
                            </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
           
            </div>          
           
           
            <div class="tab-pane fade " id="registrarse" >
             <br>
             <br>
              <div class="card card-container">
                <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
                <p id="profile-name" class="profile-name-card"></p>
                <!-- <form class="form-signin"> -->
                <?php $data = array('class' => 'form-signin', );
                echo form_open('Sistema/RegistroUser', $data); ?>
                    <span id="reauth-email" class="reauth-email"></span>
                    <label for="sel1">Usuario:</label>
                    <input class="form-control" id="user" placeholder="correo@ejemplo.com" name="username">
                    <label for="sel1">Contraseña:</label>
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="passwordlogin">
                    <div id="remember" class="checkbox">
                    </div>
                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Registrarse</button>
                </form>
            </div><!-- /card-container -->

            </div>
        </div>
    </div> <!-- /ContenedorPrincipal -->

    <!-- Footer -->
    

    <!-- jQuery -->
    <script src="<?php echo base_url('js/jquery.js')?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>

</body>

</html>
