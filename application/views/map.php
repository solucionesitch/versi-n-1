<!DOCTYPE html>
<html>
<head>
	<title>Mapa</title>
	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">
	<script type="text/javascript">
          function pregunta(){
            if (confirm('¿Estas seguro?')){
               return true;
            }else{
              return false;
            }
          } 
        </script>
</head>
<body>
	<div class="container">
		<form>

                Destino: 
                <select id="locale" name="address">
                	<?php foreach ($places as $place) {
                		echo '<option value="'.$place['lat'].', '.$place['lon'].'" selected>'.$place['nombre'].'</option>';
                	} ?>
                      <!--<option value="20.968467487410514, -89.62386425180665" selected>Hotel los cocos</option>
                      <option value="Miguel Hidalgo 47, Plutarco Elías Calles, 77090 Chetumal" selected>Parque los caimanes</option>
                      <option value="Cancun" selected>Cancun</option>
                      <option value="Av. Constituyentes del 74 no 254 Mz 066 lote 001, El Encanto, 77086 Chetumal, Q.R." selected>Chedraui</option>
                      <option value=" Chetumal, Reforma, 77018 Chetumal, Q.R." selected>Walmart</option>
                      <option value="Portal del Huajuco, 77020 Chetumal, Quintana Roo" selected>HomeDepot</option>-->     
                </select>
                <input type="button" value="Obtener ruta a esta direccion" onclick="travelToAddress();"/>
                </form>
                <div class="col-md-6" id="mapa" style="height: 450px; width: 100%;" ></div>

                <div class="col-md-6">
                	<table>
                	<tr>
                		<td>Proveedor</td>
                		<td>Acción</td>
                	</tr>
                	
                		<?php foreach ($places as $place) {
                			echo "<tr><td>".$place['nombre']."</td>";
                			echo "<td>";
                				echo form_open('User/eliminaPlace');
                				echo "<input type='hidden' value='".$place['id_place']."' name='id'>
                					  <input type='submit' value='Eliminar' class='btn btn-danger form-control' onclick='return pregunta()'>";
                			echo "</form></td></tr>";
                		} ?>
                	</tr>
                </table>
                </div>
                

                <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=
                AIzaSyA9Wpp7_aZf8tQ_Nh2odQ6aZVb9tsFjFpo"></script>
                <script type="text/javascript" >

                var map;
                var myPos;
                var directionsRenderer;
                var directionsService = new google.maps.DirectionsService();

                if (navigator && navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(geoOK, geoKO);
                } 
                else {
                    geoMaxmind();
                }
                function geoOK(position) {
                    showMap(position.coords.latitude, position.coords.longitude);
                }
                function geoMaxmind() {
                    showMap(geoip_latitude(), geoip_longitude());
                }

                function geoKO(err) {
                    if (err.code == 1) {
                        error('El usuario ha denegado el permiso para obtener informacion de ubicacion.');
                    } else if (err.code == 2) {
                        error('Tu ubicacion no se puede determinar.');
                    } else if (err.code == 3) {
                        error('TimeOut.')
                    } else {
                        error('No sabemos que pasó pero ocurrio un error.');
                    }
                }

                function showMap(lat, longi) {

                    myPos = new google.maps.LatLng(lat,longi);
                    var myOptions = {
                    zoom: 13,
                    center: myPos,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                }

                map = new google.maps.Map(document.getElementById("mapa"), myOptions);

                var marker = new google.maps.Marker({
                    position: myPos,
                    title:"Hola Mundo",
                    draggable:true,
                    animation: google.maps.Animation.DROP
                });

                marker.setMap(map);

                directionsRenderer = new google.maps.DirectionsRenderer();
                directionsRenderer.setMap(map);

                }

                function travelToAddress(){

                    //Obtenemos la direccion
                    destino=document.querySelector('#locale').value;
                    directionsService = new google.maps.DirectionsService();
                    // opciones de busqueda
                    var request = {
                        origin: myPos,
                        destination: destino,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    };

                    directionsService.route(request,getRuta);
                }

                function getRuta(result, status){

                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsRenderer.setDirections(result);
                    } else {
                        error("Ha ocurrido un error debido a : " + status);
                    }
                }

                function error(msg) {
                    alert(msg);
                }
                </script>
	</div>
</body>
</html>