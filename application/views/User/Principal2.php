<!DOCTYPE html>
<html lang="en">
    <head>
        <?php if (!isset($error)) {
          $error = null;
        } ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sistema Presupuesto</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

        <!-- Custom CSS -->
        <link href="<?php echo base_url('css/landing-page.css')?>" rel="stylesheet">

        <script src="<?php echo base_url('dist/js/bootstrap-checkbox.min.js')?>"></script>

        <!-- Custom Fonts -->
        <link href="<?php echo base_url('font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <link  type="text/css"  rel="stylesheet" href="<?php echo base_url('css/styleLogin.css')?>">

        <link href="<?php echo base_url('css/style.css')?>" rel="stylesheet" />

        <!-- jQuery -->
        <script src="<?php echo base_url('js/jquery.js')?>"></script>

        <script type="text/javascript">
          function pregunta(){
            if (confirm('¿Estas seguro?')){
               return true;
            }else{
              return false;
            }
          } 
        </script>

        <style type="text/css">
          th {text-align: left; padding-right: 1em}
          th, td.sub {background-color: #eee}
          .table {
            border: 1px solid black;
          }
          .table .st-key {
            font-weight: bold;
          }

          @media screen and (max-width:350px){
            .btn-group a button{
              font-size: 9px;
            }
          }
        </style>

        <style type="text/css">
          @charset "UTF-8";
          /* CSS Document */

          @media screen and (max-width:720px){
            table, thead, tr, th, tbody, td{
              display:block;
            }
            
            thead tr { 
              position: absolute;
              top: -9999px;
              left: -9999px;
            }
            
            td { 
              border: none;
              border-bottom: 1px solid #eee; 
              position: relative;
              padding-left: 50%; 
            }
            td:before { 
              position: absolute;
            
              top: 6px;
              left: 6px;
              width: 45%; 
              padding-right: 10px; 
              white-space: nowrap;
            }
          }
        </style>
    </head>

    <body onload="startDB()">
      <!-- Navigation -->
      <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand topnav" href="<?php echo site_url('Sistema/System')?>">Presupuesto Personal</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Opciones <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo site_url('Sistema/System')?>">Compra Tiempo Real</a>
                  </li>

                  <li>
                    <a href=<?php echo site_url('User/index')?>>Preparación para las compras</a>
                  </li>

                  <li>
                    <a href="<?php echo site_url('User/CompraLista')?>">Compra con lista</a>
                  </li>

                  <li>
                    <a href="<?php echo site_url('User/provee')?>">Proveedores</a>
                  </li>

                  <li>
                    <a href=<?php echo site_url('User/mapAgrega'); ?>>Agregar proveedor</a>
                  </li>
                </ul>
              </li>

              <li><a href="<?php echo site_url('Sistema/logout')?>">Cerrar Sesión</a></li>
            </ul>
          </div>
        <!-- /.navbar-collapse -->
        </div>
      <!-- /.container -->
      </nav>


      <!--Vista de contenedores Dinamicos -->
      <div class="container">
        <div class="tab-content">
          <div class="tab-pane fade <?php if ($funcion == 'TiempoReal') {
            echo "in active";
          } ?>" id="tiempoReal">
            <div class="container">
              <h2>Presupuesto Personal en Tiempo real <button class="btn btn-primary" onclick="location.href='<?php echo site_url('User/Informe')?>'">Emitir informe</button> <button class="btn btn-danger" onclick="location.href='<?php echo site_url('User/EliminarTiempoReal')?>'">Reestablecer Agregados</button></h2>
              <h5>Instrucciones: Agregue los gastos que realize en tiempo real, el sistema avisará cuando un pago exceda su presupuesto, asi mismo usted puede agregar una categoría de pago en caso de no estar y tambien puede emitir un informe de los pagos realizados hasta el momento.</h5>
              <?php if ($error == 'error') { ?>
                <div class="alert alert-danger" style="margin-top: 0%; margin-bottom: 0;">Has sobrepasado tu ingreso Quincenal</div>
              <?php }else if ($error == 'ok'){ ?>
                <div class="alert alert-success" style="margin-top: 0%; margin-bottom: 0;">Agregado Correctamente</div>
              <?php } ?>
              <?php $data = array('name' => 'DatosFijos', );
              echo form_open('Sistema/FijoTiempoReal', $data); ?>
                <h5>Gastos Fijos</h5>
                <table id="FijoTiempoReal" cellspacing="0" class="table">
                  <thead>
                    <tr>
                      <th>Categoria</th>
                      <th>Valor</th>
                      <th width="20%">Acción</th>
                    </tr>
                  </thead> 
                  <tbody>
                    <tr>
                      <td>
                        <select name="categoria" class="form-control" required="true">
                          <option value="" selected>Seleccione...</option>
                          <?php foreach ($CatFijos as $categoria) { ?>
                            <option value="<?php echo $categoria['Id'] ?>"><?php echo $categoria['Categoria'] ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td>
                        <input type="text" class="form-control" placeholder="$" name="precio" required="true">
                      </td>
                      <td>
                        <input type="submit" name="" class="btn btn-success form-control" value="Agregar">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>

              <?php $data = array('name' => 'DatosBasicos', );
              echo form_open('Sistema/BasicosTiempoReal', $data); ?>
                <h5>Gastos Basicos</h5>
                <table id="BasicosTiempoReal" cellspacing="0" class="table">
                  <thead>
                    <tr>
                      <th>Categoria</th>
                      <th>SubCategoria</th>
                      <th>Valor</th>
                      <th>Cantidad</th>
                      <th width="20%">Acción</th>
                    </tr>
                  </thead> 
                  <tbody>
                    <tr>
                      <td>
                        <select name="categoria" onchange="cambia_subcategoria()" class="form-control" required="true">
                          <option value="" selected>Seleccione...</option>
                          <?php foreach ($CatBasicos as $categoria) { ?>
                            <option value="<?php echo $categoria['Id'] ?>"><?php echo $categoria['Categoria'] ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td>
                        <select name="subcategoria" class="form-control" required="true">
                          <option value="">-</option>
                        </select> 
                      </td>
                      <td>
                        <input type="text" class="form-control" placeholder="$" name="precio" required="true">
                      </td>
                      <td>
                        <input type="text" class="form-control" placeholder="#" name="cantidad" required="true">
                      </td>
                      <td>
                        <input type="submit" name="" class="btn btn-success form-control" value="Agregar">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>

              <?php $data = array('name' => 'DatosOtros', );
              echo form_open('Sistema/OtrosTiempoReal', $data); ?>
                <h5>Gastos en Otros Servicios</h5>
                <table id="OtrosTiempoReal" cellspacing="0" class="table">
                  <thead>
                    <tr>
                      <th>Categoria</th>
                      <th>SubCategoria</th>
                      <th>Valor</th>
                      <th width="20%">Acción</th>
                    </tr>
                  </thead> 
                  <tbody>
                    <tr>
                      <td>
                        <select name="categoria" onchange="cambia_subcategoriaOtro()" class="form-control" required="true">
                          <option value="" selected>Seleccione...</option>
                          <?php foreach ($CatOtros as $categoria) { ?>
                            <option value="<?php echo $categoria['Id'] ?>"><?php echo $categoria['Categoria'] ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td>
                        <select name="subcategoria" class="form-control" required="true">
                          <option value="">-</option>
                        </select> 
                      </td>
                      <td>
                        <input type="text" class="form-control" placeholder="$" name="precio" required="true">
                      </td>
                      <td>
                        <input type="submit" name="" class="btn btn-success form-control" value="Agregar">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>
              <center>
              <h5>Interfaz para agregar Categorias/Subcategorías</h5>
              <div class="btn-group">
                <a href="<?php echo site_url('User/GastoFijo')?>" target="agregar"><button class="btn btn-success">Categoría Gasto fijo</button></a>
                <a href="<?php echo site_url('User/CategoriaProductoBasico')?>" target="agregar"><button class="btn btn-success">Categoría Productos básico</button></a>
                <a href="<?php echo site_url('User/SubcategoriaProductoBasico')?>" target="agregar"><button class="btn btn-success">Subcategoría Productos básico</button></a>
                <a href="<?php echo site_url('User/CategoriaOtrosServicios')?> " target="agregar"><button class="btn btn-success">Categoría Otros Servicios</button></a>
                <a href="<?php echo site_url('User/SubcategoriaOtrosServicios')?>" target="agregar"><button class="btn btn-success">Subcategoría Otros servicios</button></a>
              </div>
                <div>
                  <iframe src="" name="agregar" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>
          </div>

          <div class="tab-pane fade <?php if ($funcion == 'Lista') {
            echo "in active";
          } ?>" id="lista">
            <div class="container">
              <h2>Crear lista personalizada de productos básicos</h2>
              <?php if ($error == 'error') { ?>
                <div class="alert alert-danger" style="margin-top: 0%; margin-bottom: 0;">Has sobrepasado tu ingreso Quincenal</div>
              <?php }else if ($error == 'ok'){ ?>
                <div class="alert alert-success" style="margin-top: 0%; margin-bottom: 0;">Agregado Correctamente</div>
              <?php }

              $data = array('name' => 'DatosFijos', );
              echo form_open('User/FijoLista', $data); ?>
              <script type="text/javascript">
                var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
                var dataBase = null;

                function addAll(){
                  if(window.XMLHttpRequest) {
                        peticion_http = new XMLHttpRequest();
                      }
                      else if(window.ActiveXObject) {
                        peticion_http = new ActiveXObject("Microsoft.XMLHTTP");
                      }
                     
                      // Preparar la funcion de respuesta
                      peticion_http.onreadystatechange = muestraContenidojson;
                     
                      // Realizar peticion HTTP
                      peticion_http.open('POST', 'http://localhost:8080/proyecto/categoria/consulta', true);
                      peticion_http.send(JSON.stringify({"IdUser" : "<?php echo $this->session->userdata['Id'] ?>"}));

                      function muestraContenidojson() {
                          if(peticion_http.readyState == 4) {
                            if(peticion_http.status == 200) {
                              $datos = JSON.parse(peticion_http.responseText);
                              for (i = 0; i < $datos.lista_usuario.length; i++) { 
                                addLista($datos.lista_usuario[i].Gasto, $datos.lista_usuario[i].Valor);
                              }
                              loadAll();
                            }else{
                              alert(peticion_http.responseText);
                              loadAll();
                            }
                          }
                        }
                }

                function loadAll() {                   
                    var active = dataBase.result;
                    var data = active.transaction(["listausuario"], "readonly");
                    var object = data.objectStore("listausuario");
                    var elements = [];
                    object.openCursor().onsuccess = function (e) {
                        var result = e.target.result;
                        if (result === null || result.value.IdUser != <?php echo $this->session->userdata['Id'] ?>) {
                            return;
                        }

                        elements.push(result.value);
                        result.continue();
                    };

                    data.oncomplete = function () {
                        document.querySelector("#elementsList").innerHTML = '';
                        var outerHTML = '';
                        if (elements == ''){
                          outerHTML += '\n\
                                            <tr>\n\
                                                <td style="text-align: center;" colspan=2>No hay elementos actualmente</td>\n\
                                            </tr>';
                        }
                        for (var key in elements) {
                            outerHTML += '\n\
                                            <tr>\n\
                                                <td style="text-align: center;">' + elements[key].gasto + '</td>\n\
                                                <td style="text-align: center;">\n\
                                                    <button style="width:50%" class="btn btn-danger form-control" type="button" onclick="borra(' + elements[key].id + ')">Eliminar</button>\n\
                                                </td>\n\
                                            </tr>';
                        }
                        elements = [];
                        document.querySelector("#elementsList").innerHTML = outerHTML;
                    };
                } 

                function addLista(gasto, valor){
                    var active = dataBase.result;
                    var data = active.transaction(["listausuario"], "readwrite");
                    var object = data.objectStore("listausuario");

                    var valida = false;
                    object.openCursor().onsuccess = function(event) {
                      var cursor = event.target.result;
                      if (cursor) {
                        if (cursor.value.gasto == gasto) {
                          valida = true;
                        }else{
                          
                        }
                        cursor.continue();
                      }
                      else {
                        if (valida == true) {
                          alert('No valido');
                        }else{
                          var request = object.put({
                            gasto: gasto,
                            valor: valor,
                            IdUser: <?php echo $this->session->userdata['Id'] ?>
                          });

                          request.onerror = function (e) {
                            alert(request.error.name + '\n\n' + request.error.message);
                          };

                          data.oncomplete = function (e) {
                            alert('Objeto agregado correctamente');
                            loadAll();
                          };
                        }
                      }

                    };
                }

                function borra(id) {
                  if (confirm('¿Estas seguro?')){
                    var active = dataBase.result;
                    var data = active.transaction(["listausuario"], "readwrite");
                    var object = data.objectStore("listausuario");
                    var request = object.delete(parseInt(id));

                    request.onsuccess = function (e) {
                        alert("Eliminado");
                    };
                    request.onerror = function (e) {
                        alert("Error al eliminar");
                    };
                    loadAll();
                  }else{
                    return false;
                  }
                }

                function startDB() {
                    dataBase = indexedDB.open("presupuestopersonal", 1);

                    dataBase.onupgradeneeded = function (e) {
                        var active = dataBase.result;
                        var object = active.createObjectStore("listausuario", { keyPath : 'id', autoIncrement : true });
                        object.createIndex('gasto', 'gasto', { unique : false });
                        object.createIndex('valor', 'valor', { unique : false });
                        object.createIndex('IdUser', 'IdUser', { unique : false });
                    };

                    dataBase.onsuccess = function (e) {
                        alert('Base de datos cargada correctamente');
                        var active = e.target.result;
                        addAll();
                    };
                            
                    dataBase.onerror = function (e)  {
                        alert('Error cargando la base de datos');
                    };
                }

                function add(tipo){
                  var cond = '#cat'+tipo;
                  if (document.querySelector(cond).value == '' || document.querySelector(cond).value == '-') {
                    alert('Elemento Vacío, seleccione categoría y/o subcategoría si corresponde');
                  }else{
                    var active = dataBase.result;
                    var data = active.transaction(["listausuario"], "readwrite");
                    var object = data.objectStore("listausuario");

                    var valida = false;
                    object.openCursor().onsuccess = function(event) {
                      var cursor = event.target.result;
                      if (cursor) {
                        if (cursor.value.gasto == document.querySelector(cond).value) {
                          valida = true;
                        }else{
                          
                        }
                        cursor.continue();
                      }
                      else {
                        if (valida == true) {
                          alert('No valido');
                        }else{

                          var request = object.put({
                            gasto: document.querySelector(cond).value,
                            valor: 0,
                            IdUser: <?php echo $this->session->userdata['Id'] ?>
                          });

                          request.onerror = function (e) {
                            alert(request.error.name + '\n\n' + request.error.message);
                          };

                          data.oncomplete = function (e) {
                            document.querySelector(cond).value = '';
                            alert('Objeto agregado correctamente');
                            loadAll();
                          };
                        }
                      }
                    };
                  }
                }

                function peticion() {
                    var active = dataBase.result;
                    var transaction = active.transaction("listausuario", "readwrite");
                    var objectStore = transaction.objectStore("listausuario");

                    var listausuario = [];

                    objectStore.openCursor().onsuccess = function(event) {
                      var cursor = event.target.result;
                      if (cursor) {
                        listausuario.push(cursor.value);
                        cursor.continue();
                      }
                      else {
                        if(window.XMLHttpRequest) {
                            peticion_http = new XMLHttpRequest();
                          }
                          else if(window.ActiveXObject) {
                            peticion_http = new ActiveXObject("Microsoft.XMLHTTP");
                          }
                        peticion_http.onreadystatechange = muestraContenido;
                        peticion_http.open('POST', 'http://localhost:8080/proyecto/categoria/registro', false);
                        
                          var cade = '{';
                            for (i = 0; i < listausuario.length; i++) {
                              cade += '"' + i + '": {"Gasto" : "' + listausuario[i].gasto + '", "Valor": "' + listausuario[i].valor + '", "IdUser": "' + listausuario[i].IdUser + '"}';
                              if (i+1 < listausuario.length) {
                                cade += ', ';
                              }
                            }

                          cade += '}';
                          cade = JSON.parse(cade);
                          console.log(cade);
                          peticion_http.send(JSON.stringify(cade));
                      }
                    };
                }

                function muestraContenido() {
                          if(peticion_http.readyState == 4) {
                            if(peticion_http.status == 200) {
                              alert('Registrado con éxito');
                            }else{
                              alert(peticion_http.responseText);
                            }
                          }
                        }
              </script>

                <h5>Gastos Fijos</h5>
                <table id="FijoLista" cellspacing="0" class="table">
                  <thead>
                    <tr>
                      <th>Categoria</th>
                      <th width="20%">Acción</th>
                    </tr>
                  </thead> 
                  <tbody>
                    <tr>
                      <td>
                        <select id="catFijo" name="categoria" class="form-control" required="true">
                          <option value="" selected>Seleccione...</option>
                          <?php foreach ($CatFijos as $categoria) { ?>
                            <option value="<?php echo $categoria['Categoria'] ?>"><?php echo $categoria['Categoria'] ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td>
                        <!--<input type="submit" name="" class="btn btn-success form-control" value="Agregar" required="true">-->
                        <button class="btn btn-success form-control" onclick="add('Fijo')">Agregar</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>

              <?php $data = array('name' => 'DatosBasicosLista', );
              echo form_open('User/BasicosLista', $data); ?>
                <h5>Gastos Basicos</h5>
                <table id="BasicosLista" cellspacing="0" class="table">
                  <thead>
                    <tr>
                      <th>Categoria</th>
                      <th>SubCategoria</th>
                      <th>Acción</th>
                    </tr>
                  </thead> 
                  <tbody>
                    <tr>
                      <td>
                        <select name="categoria" onchange="cambia_subcategoriaLista()" class="form-control" required="true">
                          <option value="" selected>Seleccione...</option>
                          <?php foreach ($CatBasicos as $categoria) { ?>
                            <option value="<?php echo $categoria['Id'] ?>"><?php echo $categoria['Categoria'] ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td>
                        <select id="catBasico" name="subcategoria" class="form-control" required="true">
                          <option value="">-</option>
                        </select> 
                      </td>
                      <td>
                        <!--<input type="submit" name="" class="btn btn-success form-control" value="Agregar">-->
                         <button class="btn btn-success form-control" onclick="add('Basico')">Agregar</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>

              <?php $data = array('name' => 'DatosOtrosLista', );
              echo form_open('User/OtrosLista', $data); ?>
                <h5>Gastos en Otros Servicios</h5>
                <table id="OtrosLista" cellspacing="0" class="table">
                  <thead>
                    <tr>
                      <th>Categoria</th>
                      <th>SubCategoria</th>
                      <th width="20%">Acción</th>
                    </tr>
                  </thead> 
                  <tbody>
                    <tr>
                      <td>
                        <select name="categoria" onchange="cambia_subcategoriaOtroLista()" class="form-control" required="true">
                          <option value="" selected>Seleccione...</option>
                          <?php foreach ($CatOtros as $categoria) { ?>
                            <option value="<?php echo $categoria['Id'] ?>"><?php echo $categoria['Categoria'] ?></option>
                          <?php } ?>
                        </select>
                      </td>
                      <td>
                        <select id="catOtro" name="subcategoria" class="form-control" required="true">
                          <option value="-">-</option>
                        </select> 
                      </td>
                      <td>
                        <!--<input type="submit" name="" class="btn btn-success form-control" value="Agregar" >-->
                        <button class="btn btn-success form-control" onclick="add('Otro')">Agregar</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>

              <hr>
              <div id="elements">
                  <table class="table">
                      <center><h2>Lista generada</h2></center> 
                      <hr>
                      <thead>
                          <tr>
                              <th>Categoría</th>
                              <th>Acción</th>
                          </tr>
                      </thead>
                      <tbody id="elementsList">
                          <tr>
                              <td colspan="3">Not elements to show</td>
                          </tr>
                      </tbody>
                  </table>

                  <button class="btn btn-primary form-control" onclick="peticion()">Guardar lista</button>
              </div>



            </div>
          </div>

          <div class="tab-pane fade <?php if ($funcion == 'compraLista') {
            echo "in active";
          } ?>" id="compraLista">
            <div class="container">
              <h2>Carrito de compras para hoy</h2>
              <?php if ($error == 'elimina') { ?>
                <div class="alert alert-success" style="margin-top: 0%; margin-bottom: 0;">Eliminado Correctamente</div>
              <?php }?>
              <h5> </h5>
              <?php 
              echo form_open('User/finalCompra'); ?>
                <table id="FijoCompraLista" cellspacing="0" class="table">
                  <thead>
                    <tr>
                      <th>Gasto</th>
                      <th>Marcado</th>
                      <th width="20%">Acción</th>
                    </tr>
                  </thead> 
                  <tbody>
                  <?php foreach ($listadoPersonal as $listado) { ?>
                    <tr>
                      <td>
                        <?php echo $listado['Gasto'] ?>
                      </td>
                      <td>
                        <input type="text" class="form-control" placeholder="$" required="true" name="tex<?php echo $listado['Id']; ?>">
                      </td>
                      <td>
                        <?php echo form_open('User/Elimina', $data); ?>
                          <input type="hidden" name="gasto" value="<?php echo $listado['Id']; ?>">
                          <input type="submit" name="" class="btn btn-danger form-control" value="Eliminar" onclick="return pregunta()">
                        </form>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
                <input type="submit" name="" class="btn btn-primary" value="Emitir informe" onclick="">
                <button type="button" class="btn btn-danger" onclick="location.href='<?php echo site_url('User/EliminaLista')?>'">Eliminar Lista</button>
              </form>
            </div>
          </div>

          <div class="tab-pane fade <?php if ($funcion == 'Informe') {
            echo "in active";
          } ?>" id="Informe">
            <div class="container">
              <h2>Informe</h2>
              <h5>Lista actual de compras</h5>
              <div class="container">                
                <div class="row">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title"><strong>Resumen</strong></h3>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                          <table class="table table-condensed">
                            <thead>
                                <tr>
                                  <td ><strong>Producto/Servicio</strong></td>
                                  <td class="text-center"><strong>Subtotal</strong></td>
                                  <td class="text-right"><strong>Total</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                              <?php $total = 0;
                              foreach ($listadoCompras as $compra) { 
                                 $total += $compra['Total']?>
                                <tr>
                                  <td class="text-center"><?php echo $compra['Gasto']; ?></td>
                                  <td class="text-center"><?php echo $compra['Total']; ?></td>
                                  <td class="text-right"><?php echo $total; ?></td>
                                </tr>
                             <?php } ?>
                            </tbody>
                          </table>
                          <?php if ($Ingreso != 0) { ?>
                              <div class="alert alert-success" style="margin-top: 0%; margin-bottom: 0;">Ahorro de <?php echo $Ingreso-$total ?> en base a tu ingreso</div>                          
                          <?php }?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            </div>
          </div>

          <div class="tab-pane fade <?php if ($funcion == 'InformeDos') {
            echo "in active";
          } ?>" id="InformeDos">
            <div class="container">
              <h2>Informe</h2>
              <h5>Lista actual de compras</h5>
              <div class="container">                
                <div class="row">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title"><strong>Resumen</strong></h3>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                          <table class="table table-condensed">
                            <thead>
                                <tr>
                                  <td ><strong>Producto/Servicio</strong></td>
                                  <td class="text-center"><strong>Subtotal</strong></td>
                                  <td class="text-right"><strong>Total</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                              <?php $total = 0;
                              foreach ($listadoPersonal as $compra) { 
                                 $total += $compra['Valor']?>
                                <tr>
                                  <td class="text-center"><?php echo $compra['Gasto']; ?></td>
                                  <td class="text-center"><?php echo $compra['Valor']; ?></td>
                                  <td class="text-right"><?php echo $total; ?></td>
                                </tr>
                             <?php } ?>
                            </tbody>
                          </table>
                          <?php if ($total>$Ingreso) { ?>
                            <div class="alert alert-danger" style="margin-top: 0%; margin-bottom: 0;">Has sobrepasado tu ingreso Quincenal</div>
                          <?php }else{ ?>
                            <div class="alert alert-success" style="margin-top: 0%; margin-bottom: 0;">Ahorro de <?php echo $Ingreso-$total ?> en base a tu ingreso</div>
                            <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            </div>
          </div>

          <?php //if ($funcion == 'ofertas') { ?>
          <div class="tab-pane fade <?php if ($funcion == 'provee') {
            echo "in active";
          } ?>"" id="proveedores">
            <div class="container .col-md-12">
              <br><br><br>
              <iframe height="600px" src="<?php echo site_url('Sistema/map'); ?>" scrolling="yes" frameborder="0" width="100%"></iframe>
            </div>
          </div>

          <div class="tab-pane fade" id="agregaProveedor">
            <div class="container .col-md-12">
              <br><br><br>
              <iframe height="500px" src="<?php echo site_url('User/mapAgrega'); ?>" scrolling="no" frameborder="0" width="100%"></iframe>
            </div>
          </div>
          <?php //} ?>
        </div> <!--Final Contenedo tab -->
      </div> <!-- /ContenedorPrincipal -->

      <!-- Footer -->

      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>
      <script type="text/javascript">
        <?php foreach ($CatBasicos as $categoria) { ?>
          var sub<?php echo $categoria['Id']; ?> = new Array();
        <?php } 

        foreach ($CatBasicos as $categoria) { ?>
          <?php foreach ($SubBasicos as $subcategoria) { 
            if ($subcategoria['Id_Categoria'] == $categoria['Id']) { ?>
              sub<?php echo $categoria['Id']; ?>.push(<?php echo "'".$subcategoria['Subcategoria']."'"; ?>);
            <?php }
          } 
        } ?>

        <?php foreach ($CatOtros as $categoria) { ?>
          var subb<?php echo $categoria['Id']; ?> = new Array();
        <?php } 

        foreach ($CatOtros as $categoria) { ?>
         <?php foreach ($SubOtros as $subcategoria) { 
           if ($subcategoria['Id_Categoria'] == $categoria['Id']) { ?>
             subb<?php echo $categoria['Id']; ?>.push(<?php echo "'".$subcategoria['Subcategoria']."'"; ?>);
           <?php }
           } 
        } ?>

        function cambia_subcategoria(){
          //tomo el valor del select del pais elegido
          var categoria;
          categoria = document.DatosBasicos.categoria[document.DatosBasicos.categoria.selectedIndex].value;
          //miro a ver si el pais está definido

          if (categoria != 0) {
            //si estaba definido, entonces coloco las opciones de la provincia correspondiente.
            //selecciono el array de provincia adecuado
            mis_subcategorias = eval("sub"+categoria);
            //calculo el numero de provincias
            num_subcategorias = mis_subcategorias.length;
            //marco el número de provincias en el select
            document.DatosBasicos.subcategoria.length = num_subcategorias;
            //para cada provincia del array, la introduzco en el select
            for(i=0;i<num_subcategorias;i++){
              document.DatosBasicos.subcategoria.options[i].value = mis_subcategorias[i];
              document.DatosBasicos.subcategoria.options[i].text = mis_subcategorias[i];
            }
          }else{
            //si no había provincia seleccionada, elimino las provincias del select
            document.DatosBasicos.subcategoria.length = 1;
            //coloco un guión en la única opción que he dejado
            document.DatosBasicos.subcategoria.options[0].value = "-";
            document.DatosBasicos.subcategoria.options[0].text = "-";
          }
          //marco como seleccionada la opción primera de provincia
          document.DatosBasicos.subcategoria.options[0].selected = true;
        }

        function cambia_subcategoriaOtro(){
          //tomo el valor del select del pais elegido
          var categoria;
          categoria = document.DatosOtros.categoria[document.DatosOtros.categoria.selectedIndex].value;
          //miro a ver si el pais está definido

          if (categoria != 0) {
             //si estaba definido, entonces coloco las opciones de la provincia correspondiente.
             //selecciono el array de provincia adecuado
             mis_subcategorias = eval("subb"+categoria);
             //calculo el numero de provincias
             num_subcategorias = mis_subcategorias.length;
             //marco el número de provincias en el select
             document.DatosOtros.subcategoria.length = num_subcategorias;
             //para cada provincia del array, la introduzco en el select
             for(i=0;i<num_subcategorias;i++){
               document.DatosOtros.subcategoria.options[i].value = mis_subcategorias[i];
               document.DatosOtros.subcategoria.options[i].text = mis_subcategorias[i];
             }
          }else{
             //si no había provincia seleccionada, elimino las provincias del select
             document.DatosOtros.subcategoria.length = 1;
             //coloco un guión en la única opción que he dejado
             document.DatosOtros.subcategoria.options[0].value = "-";
             document.DatosOtros.subcategoria.options[0].text = "-";
          }
          //marco como seleccionada la opción primera de provincia
          document.DatosOtros.subcategoria.options[0].selected = true;
        }

        function cambia_subcategoriaLista(){
          //tomo el valor del select del pais elegido
          var categoria;
          categoria = document.DatosBasicosLista.categoria[document.DatosBasicosLista.categoria.selectedIndex].value;
          //miro a ver si el pais está definido

          if (categoria != 0) {
            //si estaba definido, entonces coloco las opciones de la provincia correspondiente.
            //selecciono el array de provincia adecuado
            mis_subcategorias = eval("sub"+categoria);
            //calculo el numero de provincias
            num_subcategorias = mis_subcategorias.length;
            //marco el número de provincias en el select
            document.DatosBasicosLista.subcategoria.length = num_subcategorias;
            //para cada provincia del array, la introduzco en el select
            for(i=0;i<num_subcategorias;i++){
              document.DatosBasicosLista.subcategoria.options[i].value = mis_subcategorias[i];
              document.DatosBasicosLista.subcategoria.options[i].text = mis_subcategorias[i];
            }
          }else{
            //si no había provincia seleccionada, elimino las provincias del select
            document.DatosBasicosLista.subcategoria.length = 1;
            //coloco un guión en la única opción que he dejado
            document.DatosBasicosLista.subcategoria.options[0].value = "-";
            document.DatosBasicosLista.subcategoria.options[0].text = "-";
          }
          //marco como seleccionada la opción primera de provincia
          document.DatosBasicosLista.subcategoria.options[0].selected = true;
        }

        function cambia_subcategoriaOtroLista(){
          //tomo el valor del select del pais elegido
          var categoria;
          categoria = document.DatosOtrosLista.categoria[document.DatosOtrosLista.categoria.selectedIndex].value;
          //miro a ver si el pais está definido

          if (categoria != 0) {
             //si estaba definido, entonces coloco las opciones de la provincia correspondiente.
             //selecciono el array de provincia adecuado
             mis_subcategorias = eval("subb"+categoria);
             //calculo el numero de provincias
             num_subcategorias = mis_subcategorias.length;
             //marco el número de provincias en el select
             document.DatosOtrosLista.subcategoria.length = num_subcategorias;
             //para cada provincia del array, la introduzco en el select
             for(i=0;i<num_subcategorias;i++){
               document.DatosOtrosLista.subcategoria.options[i].value = mis_subcategorias[i];
               document.DatosOtrosLista.subcategoria.options[i].text = mis_subcategorias[i];
             }
          }else{
             //si no había provincia seleccionada, elimino las provincias del select
             document.DatosOtrosLista.subcategoria.length = 1;
             //coloco un guión en la única opción que he dejado
             document.DatosOtrosLista.subcategoria.options[0].value = "-";
             document.DatosOtrosLista.subcategoria.options[0].text = "-";
          }
          //marco como seleccionada la opción primera de provincia
          document.DatosOtrosLista.subcategoria.options[0].selected = true;
        }
      </script>
  </body>

</html>