<!DOCTYPE html>
<html>

<?php $data = array('name' => 'DatosBasicos', );
echo form_open('User/BasicosLista', $data); ?>
	<label>Categoría</label>
	<select name="categoria" onchange="cambia_subcategoria()">
		<option value="0" selected>Seleccione...</option>
		<?php foreach ($CatBasicos as $categoria) { ?>
		<option value="<?php echo $categoria['Id'] ?>"><?php echo $categoria['Categoria'] ?></option>
		<?php } ?>
	</select>
	<br>
	<label>Subcategoría</label>
	<select name="subcategoria">
		<option value="-">-</option>
	</select>
	<br>
	<input type="submit" name="" value="Guardar">
</form>

<script type="text/javascript">
	<?php foreach ($CatBasicos as $categoria) { ?>
		var sub<?php echo $categoria['Id']; ?> = new Array();
	<?php } 

	foreach ($CatBasicos as $categoria) { ?>
		<?php foreach ($SubBasicos as $subcategoria) { 
			if ($subcategoria['Id_Categoria'] == $categoria['Id']) { ?>
				sub<?php echo $categoria['Id']; ?>.push(<?php echo "'".$subcategoria['Subcategoria']."'"; ?>);
			<?php }
		} 
	} ?>

	function cambia_subcategoria(){
	    //tomo el valor del select del pais elegido
	    var categoria;
	    categoria = document.DatosBasicos.categoria[document.DatosBasicos.categoria.selectedIndex].value;
	    //miro a ver si el pais está definido

	    if (categoria != 0) {
	       //si estaba definido, entonces coloco las opciones de la provincia correspondiente.
	       //selecciono el array de provincia adecuado
	       mis_subcategorias = eval("sub"+categoria);
	       //calculo el numero de provincias
	       num_subcategorias = mis_subcategorias.length;
	       //marco el número de provincias en el select
	       document.DatosBasicos.subcategoria.length = num_subcategorias;
	       //para cada provincia del array, la introduzco en el select
	       for(i=0;i<num_subcategorias;i++){
	          document.DatosBasicos.subcategoria.options[i].value = mis_subcategorias[i];
	          document.DatosBasicos.subcategoria.options[i].text = mis_subcategorias[i];
	       }
	    }else{
	       //si no había provincia seleccionada, elimino las provincias del select
	       document.DatosBasicos.subcategoria.length = 1;
	       //coloco un guión en la única opción que he dejado
	       document.DatosBasicos.subcategoria.options[0].value = "-";
	       document.DatosBasicos.subcategoria.options[0].text = "-";
	    }
	    //marco como seleccionada la opción primera de provincia
	    document.DatosBasicos.subcategoria.options[0].selected = true;
	}
</script>
</html>