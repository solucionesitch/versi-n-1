	<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">
	<br>
	<?php if ($tipo == 'CategoriaFijo') {?>
		<?php echo form_open('Sistema/GuardaNuevo'); ?>
			<table" cellspacing="0" class="table">
				<thead>
					<tr>
						<td><center>Gasto fijo a agregar</center></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="text" name="gasto" class="form-control" placeholder="Escriba el gasto"></td>
						<td><input type="submit" class="btn btn-primary form-control" value="Guardar"></td>
						<input type="hidden" name="tipo" value="categoria_fijos">
					</tr>
				</tbody>
			</table>
		</form>
	<?php }elseif ($tipo == 'CategoriaBasico') { ?>
		<?php echo form_open('Sistema/GuardaNuevo'); ?>
			<table" cellspacing="0" class="table">
				<thead>
					<tr>
						<td><center>Producto básico a agregar</center></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="text" name="gasto" class="form-control" placeholder="Escriba el gasto"></td>
						<td><input type="submit" class="btn btn-primary form-control" value="Guardar"></td>
						<input type="hidden" name="tipo" value="categoria_basicos">
					</tr>
				</tbody>
			</table>
		</form>
	<?php }elseif ($tipo == 'SubcategoriaBasico') {?>
		<?php echo form_open('Sistema/GuardaNuevoAlter'); ?>
			<table" cellspacing="0" class="table">
				<thead>
					<tr>
						<td><center>Subproducto básico a agregar</center></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<select name="categoria" class="form-control">
	                          <option value="0" selected>Seleccione...</option>
	                          <?php foreach ($CatBasicos as $categoria) { ?>
	                            <option value="<?php echo $categoria['Id'] ?>"><?php echo $categoria['Categoria'] ?></option>
	                          <?php } ?>
	                        </select>
						</td>
						<td><input type="text" name="gasto" class="form-control" placeholder="Escriba el gasto"></td>
						<td><input type="submit" class="btn btn-primary form-control" value="Guardar"></td>
						<input type="hidden" name="tipo" value="subcategoria_basicos">
					</tr>
				</tbody>
			</table>
		</form>
	<?php }elseif ($tipo == 'CategoriaOtros') { ?>
		<?php echo form_open('Sistema/GuardaNuevo'); ?>
			<table" cellspacing="0" class="table">
				<thead>
					<tr>
						<td><center>Otro servicio a agregar</center></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="text" name="gasto" class="form-control" placeholder="Escriba el gasto"></td>
						<td><input type="submit" class="btn btn-primary form-control" value="Guardar"></td>
						<input type="hidden" name="tipo" value="categoria_otros">
					</tr>
				</tbody>
			</table>
		</form>
	<?php }else{ ?>
		<?php echo form_open('Sistema/GuardaNuevoAlter'); ?>
			<table" cellspacing="0" class="table">
				<thead>
					<tr>
						<td><center>Subservicio a agregar</center></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<select name="categoria" class="form-control">
	                          <option value="0" selected>Seleccione...</option>
	                          <?php foreach ($CatOtros as $categoria) { ?>
	                            <option value="<?php echo $categoria['Id'] ?>"><?php echo $categoria['Categoria'] ?></option>
	                          <?php } ?>
	                        </select>
						</td>
						<td><input type="text" name="gasto" class="form-control" placeholder="Escriba el gasto"></td>
						<td><input type="submit" class="btn btn-primary form-control" value="Guardar"></td>
						<input type="hidden" name="tipo" value="subcategoria_otros">
					</tr>
				</tbody>
			</table>
		</form>
		<?php } ?>
