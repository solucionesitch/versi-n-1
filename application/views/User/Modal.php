<!DOCTYPE html>
<html lan ="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('css/landing-page.css')?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url('font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic"                 rel="stylesheet" type="text/css">
    <link  type="text/css"  rel="stylesheet" href="<?php echo base_url('css/styleLogin.css')?>">

    <script src="<?php echo base_url('js/jquery.js')?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>  

    <script>
            document
        $(document).ready(function(){
            $('#myModal').modal('show')
        });
    
    </script>
</head>
<body>   
<div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Welcome</h4>
      </div>

      <?php $data = array('class' => 'form-horizontal', );
      echo form_open('Sistema/Recibe', $data); ?>
        <div class="modal-body">
            <div class="form-group">
              <label for ="Nombre" class="control-label col-md-4" >Ingresar Monto Quincenal</label>
                <div class=" col-md-5">
                  <input class="form-control" id="clvAlumno" type="text" placeholder="$" name="monto" />
                </div>
            </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="location.href='<?php echo site_url('Sistema/logout')?>'">Salir</button>
          <input type="submit" name="" value="Continuar" class="btn btn-primary">
        </div>

      </form>
    </div>
  </div>
</div>
        
    
           
</body>
</html>