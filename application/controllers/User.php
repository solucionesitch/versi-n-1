<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('LoginModel');
		$this->load->model('SistemModel');
		$this->load->library('session');

	}

	public function mapAgrega()
	{
		$this->load->view('principalMarker');
	}

	public function eliminaPlace()
	{
		$id = $_POST['id'];
		$this->SistemModel->EliminaPlace($id);
		redirect('Sistema/map');
	}

	public function posteo()
	{
		$lat = $_POST['lat'];
		$lon = $_POST['lon'];
		$nombre = $_POST['nombre'];

		$places = $this->SistemModel->Obtener_Datos('places');
		$valida = false;
		foreach ($places as $place) {
			if (($place['lat'] == $lat && $place['lon'] == $lon) || $place['nombre'] == $nombre) {
				$valida = true;
				echo "Lugar ya ingresado";
			}else{
				$valida = false;
			}
		}

		if ($valida == false) {
			$this->SistemModel->InsertarPlace($lat, $lon, $nombre, $this->session->userdata['Id']);
		}
		redirect('User/mapAgrega');
	}

	public function index(){
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'Lista',
					  'listadoPersonal' => array(),
					  'error' => false,
					  'listadoCompras' => $listadoCompras);

		$this->load->view('User/Principal2',$data);
	}

	public function provee(){
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'provee',
					  'listadoPersonal' => array(),
					  'error' => false,
					  'listadoCompras' => $listadoCompras);

		$this->load->view('User/Principal2',$data);
	}

	public function CompraLista(){
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'lista_usuario');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'compraLista',
					  'listadoPersonal' => $listadoPersonal,
					  'error' => false,
					  'listadoCompras' => $listadoCompras);

		$this->load->view('User/Principal2',$data);
	}

	public function SystemOk(){
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'lista_usuario');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'Lista',
					  'agregar' => false,
					  'listadoPersonal' => array(),
					  //'error' => 'ok',
					  'listadoCompras' => $listadoCompras);

		$this->load->view('User/Principal2',$data);
	}

	public function SystemOkElimina(){
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'lista_usuario');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'compraLista',
					  'listadoPersonal' => $listadoPersonal,
					  'error' => 'elimina',
					  'listadoCompras' => $listadoCompras);

		$this->load->view('User/Principal2',$data);
	}

	public function FijoLista()
	{
		/*$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		foreach ($listadoCatFijos as $fijos) {
			if ($fijos['Id'] == $_POST['categoria']) {
				$cat = $fijos['Categoria'];
			}
		}
		$this->SistemModel->Insertar_Lista($cat, 0, $this->session->userdata['Id']);**/
		$this->SystemOk();
	}

	public function BasicosLista()
	{

		//$this->SistemModel->Insertar_Lista($_POST['subcategoria'], 0, $this->session->userdata['Id']);
		$this->SystemOk();
	}

	public function OtrosLista()
	{

		/*$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');

		foreach ($listadoSubOtros as $subcategoria) {
			if ($subcategoria['Id_Categoria'] == $_POST['categoria'] && $subcategoria['Subcategoria'] == $_POST['subcategoria']) {
				$subcategoriA = $subcategoria['Id'];
			}
		}

		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		foreach ($listadoCatOtros as $otros) {
			if ($otros['Id'] == $_POST['categoria']) {
			}
		}


		$this->SistemModel->Insertar_Lista($_POST['subcategoria'], 0, $this->session->userdata['Id']);*/
		$this->SystemOk();
	}

	public function GastoFijo()
	{
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->Obtener_Datos('lista_usuario');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'listadoPersonal' => $listadoPersonal,
					  'tipo' => 'CategoriaFijo',);

		$this->load->view('User/Agrega',$data);
	}

	public function CategoriaProductoBasico()
	{
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->Obtener_Datos('lista_usuario');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'listadoPersonal' => $listadoPersonal,
					  'tipo' => 'CategoriaBasico',);

		$this->load->view('User/Agrega',$data);
	}

	public function SubcategoriaProductoBasico()
	{
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->Obtener_Datos('lista_usuario');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'listadoPersonal' => $listadoPersonal,
					  'tipo' => 'SubcategoriaBasico',);

		$this->load->view('User/Agrega',$data);
	}

	public function CategoriaOtrosServicios()
	{
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->Obtener_Datos('lista_usuario');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'listadoPersonal' => $listadoPersonal,
					  'tipo' => 'CategoriaOtros',);

		$this->load->view('User/Agrega',$data);
	}

	public function SubcategoriaOtrosServicios()
	{
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->Obtener_Datos('lista_usuario');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'listadoPersonal' => $listadoPersonal,
					  'tipo' => 'SubcategoriaOtros',);

		$this->load->view('User/Agrega',$data);
	}

	public function Informe()
	{
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->Obtener_Datos('lista_usuario');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');
		$user = $this->SistemModel->filtro($this->session->userdata['Id']);
		foreach ($user as $us) {
			$ingreso = $us['Ingreso'];
		}

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'Informe',
					  'listadoPersonal' => $listadoPersonal,
					  'error' => false,
					  'listadoCompras' => $listadoCompras,
					  'Ingreso' => $ingreso);

		$this->load->view('User/Principal2',$data);
	}

	public function Elimina(){
		$this->SistemModel->ELiminar($_POST['gasto']);
		$this->SystemOkElimina();
	}

	public function finalCompra(){
		$listas = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'lista_usuario');
		foreach ($listas as $elemento) {
			$cade = "tex".$elemento['Id'];
			$this->SistemModel->Actualiza_lista($elemento['Id'], $_POST[$cade]);
		}

		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoPersonal = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'lista_usuario');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');
		$user = $this->SistemModel->filtro($this->session->userdata['Id']);
		foreach ($user as $us) {
			$ingreso = $us['Ingreso'];
		}

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'InformeDos',
					  'listadoPersonal' => $listadoPersonal,
					  'error' => false,
					  'listadoCompras' => $listadoCompras,
					  'Ingreso' => $ingreso);

		$this->load->view('User/Principal2',$data);
	}

	public function EliminaLista(){
		$listas = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'lista_usuario');
		foreach ($listas as $elemento) {
			$this->SistemModel->ELiminar($elemento['Id']);
			
		}
		$this->SystemOkElimina();
	}

	public function EliminarTiempoReal(){
		$listas = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');
		foreach ($listas as $elemento) {
			$this->SistemModel->ELiminarDos($elemento['Id']);
		}
		$this->SistemModel->Actualiza_Temp($this->session->userdata['Id'], 0);
		$this->Informe();
	}
}