<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->model('LoginModel');
		$this->load->model('SistemModel');
		$this->load->library('session');

		/*$info_usuario = array(
			'Id' => '2'
		);
		$this->session->set_userdata($info_usuario);*/
	}

	public function index()
	{
		$this->load->view('Index');
	}

	public function map()
	{
		$places = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'places');
		$data = array('places' => $places, );
		$this->load->view('map', $data);
	}

	public function Registro()
	{
		$this->load->view('Login/Registro');
	}

	public function RegistroUser()
	{
		$this->LoginModel->Insertar_Usuario($_POST['username'], $_POST['passwordlogin']);
		echo "Registrado con éxito";
	}

	public function User()
	{
		if(!isset($_POST['entrar'])){ //Si no recibimos ningún valor proveniente del formulario, significa que el usuario recién ingresa.   
			$this->load->view('Index'); //Por lo tanto le presentamos la pantalla del formulario de ingreso.
      	}else{
      		/*echo $_POST['username'];
			echo $_POST['passwordlogin'];*/
			$ExisteUsuarioyPassword=$this->LoginModel->ValidarUsuario($_POST['username'],$_POST['passwordlogin']); //comprobamos que el usuario exista en la base de datos y la password ingresada sea correcta
		    if($ExisteUsuarioyPassword){
		        $listadoUserPass = $this->LoginModel->Obtener_UserPass();
		        foreach ($listadoUserPass as $user) {
		           	if ($user['username'] == $_POST['username']) {
		           		//echo $user['Id_UserPass'];
		           		$info_usuario = array(
							'Id' => $user['Id_UserPass'],
							'Ingreso' => $user['Ingreso']
						);
						$this->session->set_userdata($info_usuario);

						//$this->SystemAlter($user['Ingreso']);
						redirect('Sistema/SystemAlter');
		           	}
		        }
		    }else{ //Si no logró validar
		        $data['error']="Usuario o password incorrecta, por favor vuelva a intentar";
		        $this->load->view('index',$data); //Lo regresamos a la pantalla de login y pasamos como parámetro el mensaje de error a presentar en pantalla
		    }
		}
	}

	public function logout(){
		$this->session->unset_userdata('Id');
		$this->session->unset_userdata('Ingreso');
		//$this->User();
		redirect('Sistema/User');
	}

	public function SystemAlter(){
		if ($this->session->userdata['Ingreso'] == null) {
			$this->load->view('User/Modal');
		}else{
			$this->System();
		}
		
	}

	public function Recibe(){
		$this->SistemModel->Actualiza_User($this->session->userdata['Id'], $_POST['monto']);
		$this->System();
	}

	public function System(){
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'TiempoReal',
					  'agregar' => false,
					  'listadoPersonal' => array(),
					  'error' => false,
					  'listadoCompras' => $listadoCompras);

		$this->load->view('User/Principal2',$data);
	}

	public function SystemError(){
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');
		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'TiempoReal',
					  'agregar' => false,
					  'listadoPersonal' => array(),
					  'error' => 'error',
					  'listadoCompras' => $listadoCompras);

		$this->load->view('User/Principal2',$data);
	}

	public function SystemOk(){
		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		$listadoCatOtros = $this->SistemModel->Obtener_Datos('categoria_otros');
		$listadoSubOtros = $this->SistemModel->Obtener_Datos('subcategoria_otros');
		$listadoCompras = $this->SistemModel->filtroAlter($this->session->userdata['Id'], 'compratiemporeal');

		$data = array('CatBasicos' => $listadoCatBasicos, 
					  'SubBasicos' => $listadoSubBasicos,
					  'CatFijos' => $listadoCatFijos,
					  'CatOtros' => $listadoCatOtros,
					  'SubOtros' => $listadoSubOtros,
					  'funcion' => 'TiempoReal',
					  'agregar' => false,
					  'listadoPersonal' => array(),
					  'error' => 'ok',
					  'listadoCompras' => $listadoCompras);

		$this->load->view('User/Principal2',$data);
	}

	public function FijoTiempoReal()
	{
		$listadoCatFijos = $this->SistemModel->Obtener_Datos('categoria_fijos');
		foreach ($listadoCatFijos as $fijos) {
			if ($fijos['Id'] == $_POST['categoria']) {
				$cat = $fijos['Categoria'];
			}
		}

		$filtrado = $this->SistemModel->filtro($this->session->userdata['Id']);
		foreach ($filtrado as $filter) {
			if ($filter['temp']+$_POST['precio'] > $filter['Ingreso']) {
				$this->SystemError();
			}else{
				$this->SistemModel->Insertar_Compras($cat, $_POST['precio'], $this->session->userdata['Id']);
				$this->SistemModel->Actualiza_Temp($this->session->userdata['Id'], $filter['temp']+$_POST['precio']);
				$this->SystemOk();
			}
		}
	}

	public function BasicosTiempoReal()
	{

		$listadoSubBasicos = $this->SistemModel->Obtener_Datos('subcategoria_basicos');

		foreach ($listadoSubBasicos as $subcategoria) {
			if ($subcategoria['Id_Categoria'] == $_POST['categoria'] && $subcategoria['Subcategoria'] == $_POST['subcategoria']) {
				$subcategoriA = $subcategoria['Id'];
			}
		}

		$listadoCatBasicos = $this->SistemModel->Obtener_Datos('categoria_basicos');
		foreach ($listadoCatBasicos as $basicos) {
			if ($basicos['Id'] == $_POST['categoria']) {
				$cat = $basicos['Categoria'];
			}
		}

		$filtrado = $this->SistemModel->filtro($this->session->userdata['Id']);
		foreach ($filtrado as $filter) {
			if ($filter['temp']+$_POST['precio'] > $filter['Ingreso']) {
				$this->SystemError();
			}else{
				$this->SistemModel->Insertar_Compras($_POST['subcategoria'], $_POST['precio']*$_POST['cantidad'], $this->session->userdata['Id']);
				$this->SistemModel->Actualiza_Temp($this->session->userdata['Id'], $filter['temp']+$_POST['precio']);
				$this->SystemOk();
			}
		}
	}

	public function OtrosTiempoReal()
	{
		$filtrado = $this->SistemModel->filtro($this->session->userdata['Id']);
		foreach ($filtrado as $filter) {
			if ($filter['temp']+$_POST['precio'] > $filter['Ingreso']) {
				$this->SystemError();
			}else{
				$this->SistemModel->Insertar_Compras($_POST['subcategoria'], $_POST['precio'], $this->session->userdata['Id']);
				$this->SistemModel->Actualiza_Temp($this->session->userdata['Id'], $filter['temp']+$_POST['precio']);
				$this->SystemOk();
			}
		}
	}

	public function GuardaNuevo(){
		$this->SistemModel->Insertar_Nuevo($_POST['tipo'], $_POST['gasto']);
		echo "Actualize para reflejar cambios";
	}

	public function GuardaNuevoAlter(){
		$this->SistemModel->Insertar_NuevoALter($_POST['tipo'], $_POST['gasto'], $_POST['categoria']);
		echo "Actualize para reflejar cambios";
	}
}