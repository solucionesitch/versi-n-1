<?php
class LoginModel extends CI_Model{
   function ValidarUsuario($username,$password){ //Consulta Mysql para buscar en la tabla Usuario aquellos usuarios que coincidan con el mail y password ingresados en pantalla de login	
        $query = $this->db->where('username',$username); 
        $query = $this->db->where('password',$password);
        $query = $this->db->get('user_pass');//La consulta se efectúa mediante Active Record. Una manera alternativa, y en lenguaje más sencillo, de generar las consultas Sql.
        return $query->row();//Devolvemos al controlador la fila que coincide con la búsqueda. (FALSE en caso que no existir coincidencias)      	 
   }

   function Insertar_Usuario($username, $password){
        $data = array(
            'username' => $username,
            'password' => $password,
            'temp' => 0
          );
        $this->db->insert('user_pass', $data);
    }

    function Obtener_UserPass(){
        $query = $this->db->get('user_pass');
        return $query->result_array();
    }
}
?> 