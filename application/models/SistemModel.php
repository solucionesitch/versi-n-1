<?php
class SistemModel extends CI_Model{

    function Obtener_Datos($tabla){
       $query = $this->db->get($tabla);
       return $query->result_array();
    }

    function Insertar_Compras($Gasto, $Total, $Id){
      $data = array(
        'Gasto' => $Gasto,
        'Total' => $Total,
        'IdUser' => $Id);
      $this->db->insert('compratiemporeal', $data);
    }

    function Insertar_Lista($Gasto, $Valor, $Id){
      $data = array(
        'Gasto' => $Gasto,
        'Valor' => $Valor,
        'IdUser' => $Id);
      $this->db->insert('lista_usuario', $data);
    }

    function InsertarPlace($lat, $lon, $nombre, $id){
      $data = array(
        'nombre' => $nombre,
        'lat' => $lat,
        'lon' => $lon,
        'IdUser' => $id);
      $this->db->insert('places', $data);
    }

    function Insertar_Nuevo($Tabla, $Gasto){
      $data = array(
        'Categoria' => $Gasto);
      $this->db->insert($Tabla, $data);
    }

    function Insertar_NuevoAlter($Tabla, $Gasto, $cat){
      $data = array(
        'Subcategoria' => $Gasto,
        'Id_Categoria' => $cat,);
      $this->db->insert($Tabla, $data);
    }

    function Actualiza_User($Id_UserPass, $Ingreso) {
      $data = array(
          'Ingreso' => $Ingreso
      );
      $this->db->where('Id_UserPass', $Id_UserPass);
      return $this->db->update('user_pass', $data);
    }

    function Actualiza_Temp($Id_UserPass, $temp) {
      $data = array(
          'temp' => $temp
      );
      $this->db->where('Id_UserPass', $Id_UserPass);
      return $this->db->update('user_pass', $data);
    }

    function Actualiza_lista($Id, $valor) {
      $data = array(
          'Valor' => $valor
      );
      $this->db->where('Id', $Id);
      return $this->db->update('lista_usuario', $data);
    }

    function filtro($Id_UserPass) {
      $query = $this->db->where('Id_UserPass', $Id_UserPass);
      $query = $this->db->get("user_pass");
      return $query->result_array();
    }

    function filtroAlter($Id_UserPass, $tabla) {
      $query = $this->db->where('IdUser', $Id_UserPass);
      $query = $this->db->get($tabla);
      return $query->result_array();
    }

    function filtroLista($Id, $tabla) {
      $query = $this->db->where('Id', $Id);
      $query = $this->db->get($tabla);
      return $query->result_array();
    }

    function Eliminar($Id) {
      $this->db->where('Id',$Id);
      return $this->db->delete('lista_usuario');
    }

    function EliminaPlace($Id) {
      $this->db->where('id_place',$Id);
      return $this->db->delete('places');
    }

    function EliminarDos($Id) {
      $this->db->where('Id',$Id);
      return $this->db->delete('compratiemporeal');
    }
}
?> 